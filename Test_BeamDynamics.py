import BeamDynamics as bd


bd.convert_irina_distr_to_standard_df(
    'Geant4/FcceeTarget_StartingExample/run_Injector/ex_gen1.dat',
    saveStandardCsv=True
)
